<?php
namespace ShrutiAmbab\HelloPage\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('contactData'),
                'user_id',
                [
                    'type' => Table::TYPE_INTEGER,
                    'nullable' => false,
                    'identity'=>true,
                    'primary'=>true,
                    'unsigned' => true,
                    'comment' => 'index field'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('contactData'),
                'created_at',
                [
                    'type' => Table::TYPE_TIMESTAMP,
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT,
                    'comment' => 'created at field'
                ]
            );
        }
        $setup->endSetup();
    }
}