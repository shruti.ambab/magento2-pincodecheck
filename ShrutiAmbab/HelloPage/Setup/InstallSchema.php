<?php

namespace ShrutiAmbab\HelloPage\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{
   
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
          /**
          * Create table 'contactData'
          */
          $table = $setup->getConnection()
              ->newTable($setup->getTable('contactData'))
              ->addColumn(
                  'name',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  25,
                  ['nullable' => false],
                  'Name'
              )
              ->addColumn(
                  'email',
                  \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  255,
                  ['nullable' => false ],
                    'Email'
              )
              ->addColumn(
                'contact',
                \Magento\Framework\DB\Ddl\Table::TYPE_NUMERIC,
                255,
                ['nullable' => false,'unsigned' => true],
                  'Contact'
              );
          $setup->getConnection()->createTable($table);
      }
}
