<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace ShrutiAmbab\HelloPage\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Upgrade Data script
 */

class UpgradeData implements UpgradeDataInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $table = $setup->getTable('contactData');
            $setup->getConnection()
                ->insertForce($table, ['name' => 'neha', 'email' => 'neha97@gmail.com','contact' => '9765562888']);

            // $setup->getConnection()
            //      ->update($table, ['name' => 'Pooja updated'], 'name=Pooja');
        }
        $setup->endSetup();
    }
}