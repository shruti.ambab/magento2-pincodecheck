<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace ShrutiAmbab\HelloPage\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
          $data = [
              ['name' => 'Pooja',
              'email' => 'pooja95@gmail.com',
              'contact' => '8421762954']
          ];
          foreach ($data as $bind) {
              $setup->getConnection()->insertForce($setup->getTable('contactData'), $bind);
          }
    }
}
