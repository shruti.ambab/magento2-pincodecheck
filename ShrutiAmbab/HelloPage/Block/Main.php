<?php
namespace ShrutiAmbab\HelloPage\Block;
use Magento\Framework\view\Element\Template;


class Main extends Template
{    
    protected function _prepareLayout()
    {
	    $this->setMessage('Hello Again World');
    }
    public function getFormTitle()
    {
        return 'Contact Form';
    }
}

