<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Ui\Component\MassAction\Filter;
use ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode\CollectionFactory;
use Magento\Backend\App\Action\Context;

class MassDelete extends \Magento\Backend\App\Action {

    protected $_filter;

    protected $_collectionFactory;

    public function __construct(Filter $filter, CollectionFactory $collectionFactory, Context $context) 
    {
        $this->_filter            = $filter;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute() {
        try{ 

            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            //echo "<pre>";
            //print_r($logCollection->getData());
            //exit;
            $collectionSize = $collection->getSize();
            foreach ($collection as $item) {
                $item->delete();
            }
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));
        }catch(Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('pincodechecka/index/index'); //Redirect Path
    }

    //  /**
    //  * is action allowed
    //  *
    //  * @return bool
    //  */
    // protected function _isAllowed() {
    //     return $this->_authorization->isAllowed('Prince_PincodeChecker::view');
    // }
}