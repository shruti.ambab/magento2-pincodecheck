<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;


Class Edit extends \Magento\Backend\App\Action //remember to write the class name same as file name
{
    // const ADMIN_RESOURCE = 'Index';

    protected $pageFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {          
        $id = $this->getRequest()->getParam('id');

        $page_object = $this->pageFactory->create();
        $page_object->getConfig()->getTitle()->prepend(__('Pincode (ID-'.$id.')'));
        return $page_object;
    }    
}