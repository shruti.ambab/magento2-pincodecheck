<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use ShrutiAmbab\PincodeCheck\Model\PincodeFactory;
use Magento\Backend\App\Action\Context;

Class Delete extends \Magento\Backend\App\Action {

    protected $_pincodeFactory;
    protected $messageManager;

    public function __construct(
        Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager,   
        PincodeFactory $pincodeFactory
    ) 
    {
        $this->_pincodeFactory = $pincodeFactory;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute() 
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $model = $this->_pincodeFactory->create();
            $model->load($id);
            $model->delete();
            $this->_redirect('pincodechecka/index/index');
            $this->messageManager->addSuccess(__('Pincode record (ID- '.$id.') deleted.'));
        } catch (\Exception $e) {
            $this->_redirect('pincodechecka/index/index');
            $this->messageManager->addError($e->getMessage());
        }
    }
}