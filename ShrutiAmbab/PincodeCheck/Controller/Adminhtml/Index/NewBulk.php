<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;


Class NewBulk extends \Magento\Backend\App\Action //remember to write the class name same as file name
{


    protected $pageFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {          

        $page_object = $this->pageFactory->create();
        $page_object->getConfig()->getTitle()->prepend(__('Import Pincodes'));
        return $page_object;
    }    
}