define([
    "jquery",
    'mage/mage'
], function($){
        "use strict";
        return function(config, element) {
              element.onclick = function(){  
                $("#pincode").keydown(function(event){ //will prevent user from entering spaces
                    if(event.keyCode == 32){
                        event.preventDefault();
                        return false;
                    }
                });


                let pincode = $('#pincode').val();
                if(pincode==''){
                    $("#msg").html("Please enter pincode first.");
                    $("#msg").css({"color":"red","margin-bottom": "20px","margin-up": "10px"});
                }
                else if(!$.isNumeric(pincode)){	
                    $("#msg").html("Only Digits are allowed.Please see the example.");
                    $("#msg").css({"color":"red","margin-bottom": "20px","margin-up": "10px"});
                }
                else if(!(Number.isInteger(+pincode))) { //+ for parsing i/p into integer datatype in case its not
                    $("#msg").html("Pincode needs to be an integer.Please see the example.");
                    $("#msg").css({"color":"red","margin-bottom": "20px","margin-up": "10px"});
                }
                else if(pincode.length!=6){
                    $("#msg").html("Pincode needs to be 6 digits only.Please see the example.");
                    $("#msg").css({"color":"red","margin-bottom": "20px","margin-up": "10px"});
                }
                else if(pincode.charAt(0)==0){
                    $("#msg").html("Pincode can not start with zero.");
                    $("#msg").css({"color":"red","margin-bottom": "20px","margin-up": "10px"});
                }
                else{
                    $.ajax({ //add datatype as json
                        url: "pincodecheckf/front/ajax",
                        //dataType: 'application/json',
                        type: "POST",
                        //showLoader: true,
                        data: {pincode_is : pincode},
                        beforeSend: function(){
                            $('#ajaxloader').show();
                            $("#check").attr("disabled", true);
                        },
                        complete: function(){
                            $('#ajaxloader').hide();
                            $('#check').removeAttr("disabled");
                        },
                        success : function (result){
                            //let json = JSON.parse(result);
                            //console.log(json.status);
                            //console.log(result.status);
                            if(result.status=="success"){
                                $("#msg").html(result.message);
                                $("#msg").css({"color":"blue","margin-bottom": "20px","margin-up": "10px"});
                            }
                            else if(result.status=="error"){
                                $("#msg").html(result.message);
                                $("#msg").css({"color":"red","margin-bottom": "20px","margin-up": "10px"});
                            }
                        }
                    });
                }
            }
        }
    }
)

// success : function (result){
//     if(result=="yes"){
//         $("#msg").html("<div class='validation' style='color:blue;margin-bottom: 20px;margin-up: 10px;'>Delivery is Available !</div>");
//     }
//     else if(result=="no"){
//         $("#msg").html("<div class='validation' style='color:red;margin-bottom: 20px;margin-up: 10px;'>Sorry, Delivery is Not Available.</div>");
//     }
// }
