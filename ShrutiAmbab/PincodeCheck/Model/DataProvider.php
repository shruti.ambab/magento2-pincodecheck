<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ShrutiAmbab\PincodeCheck\Model;

use ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $collection;
    protected $_loadedData;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $pincodeCollectionFactory,
        array $meta = [],
        array $data = []
    ){
        $this->collection = $pincodeCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();
    
        foreach ($items as $pincode) {
            $this->loadedData[$pincode->getId()]['pincode'] = $pincode->getData();
        }
        return $this->loadedData;

    }
}


