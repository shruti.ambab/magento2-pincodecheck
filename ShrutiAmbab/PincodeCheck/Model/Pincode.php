<?php

namespace ShrutiAmbab\PincodeCheck\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class Pincode extends AbstractModel implements IdentityInterface
{
   
    const CACHE_TAG = 'shrutiambab_pincodecheck_index';
	protected $_cacheTag = 'shrutiambab_pincodecheck_index';
	protected $_eventPrefix = 'shrutiambab_pincodecheck_index';

    protected function _construct()
    {
        $this->_init('ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}