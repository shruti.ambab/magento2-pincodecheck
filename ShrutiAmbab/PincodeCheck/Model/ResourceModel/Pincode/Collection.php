<?php
namespace ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'shrutiambab_pincodecheck_index_collection';
	protected $_eventObject = 'pincode_collection';

	
	protected function _construct()
	{
		$this->_init('ShrutiAmbab\PincodeCheck\Model\Pincode', 'ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode');
	}

}