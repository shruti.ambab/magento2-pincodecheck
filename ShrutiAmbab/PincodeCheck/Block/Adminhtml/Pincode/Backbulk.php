<?php

namespace ShrutiAmbab\PincodeCheck\Block\Adminhtml\Pincode;


class Backbulk extends \Magento\Backend\Block\Widget\Container
{
    public function _construct()
    {    
        parent::_construct();
    
        $this->addButton(
            'my_back_button',
            [
                'label' => __('Back'),
                'onclick' => 'setLocation(\'' . $this->getUrl('pincodechecka/index/index') . '\')',
                'class' => 'back'
            ],
            -1
        );
    }
}