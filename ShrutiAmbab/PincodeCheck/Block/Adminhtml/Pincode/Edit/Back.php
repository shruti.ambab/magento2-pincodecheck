<?php
namespace ShrutiAmbab\PincodeCheck\Block\Adminhtml\Pincode\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use \Magento\Backend\Block\Widget\Form\Container;


class Back extends GenericButton implements ButtonProviderInterface
{
   
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back secondary',
            'sort_order' => 10
        ];
    }

   
    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }
}
