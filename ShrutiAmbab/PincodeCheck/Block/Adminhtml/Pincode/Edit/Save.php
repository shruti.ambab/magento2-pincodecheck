<?php
namespace ShrutiAmbab\PincodeCheck\Block\Adminhtml\Pincode\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Save extends GenericButton implements ButtonProviderInterface
{

    public function getButtonData()
    {
        return
        [
            'label' => __('Save'),
            'class' => 'save primary',
            'on_click' => '',
            'sort_order' => 80,
            'data_attribute' => [
                //'mage-init' => ['button' => ['event' => 'save']],
                'mage-init' => '{"pvalidation":{}}',
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }

    public function getSaveUrl()
    {
        $id=parent::getId();
        return $this->getUrl('*/*/saveButton') ;
    }
}